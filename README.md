
# Darchive
Darchive is a json format for archiving a discord channel

Currently the current version is v3 and the format is not heavily standardised beyond the structs in this repository, this repository mostly exists separate to the bot it was made for to help ease standardisation and to serve as a reference for anything external that may support this format

See the darchiveV`x` sub-folders for the actual archive data structs, this root folder mostly just contains some common functions that are likely to be shared independently from the versions
DarchiveIdentify is a thing that exists for future use in the case a future version introduces backwards incompatible changes, or otherwise evolves to be more than just a json file that can be gzipped

## Viewers
Currently there are not any options for a polished or complete viewer for this archive format

you can find a prototype / unfinished HTML formatter for this format here
https://codeberg.org/evieDelta/darchiveViewer
it's functional enough to create something human readable though be aware that it only supports the bare minimum to be able to read plain text, no extra formatting is supported by it as i don't have the time to properly finish it

## Version history
#### Version 3.1
   - Due to change in discordgo there is a slight change in the structs in darchivev3, this shouldn't impact the json files
#### Version 3 
   - Fixes a typo in version 2 where `channel` was called `channels` 
   - `Current, Common`
   - (The majority of files that exist currently and any new ones made as of writing this are of v3)
   
#### Version 2
   - Introduces archiveVersion and information about the channel that was saved and at what time it was saved
   - `Deprecated, None known to exist`
   - (There are no known files in this version that were made prior to the introduction of v3)

#### Version 1
   - Introduction, only includes messages and no other information, does not contain any version number
   - `Deprecated, Rare`
   - (There are only a couple files known to exist in this version)

