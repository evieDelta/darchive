module codeberg.org/eviedelta/darchive

go 1.16

require (
	github.com/bwmarrin/discordgo v0.24.0
	github.com/pkg/errors v0.9.1
)
