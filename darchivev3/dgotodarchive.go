package darchivev3

import "github.com/bwmarrin/discordgo"

// DgoToDcMessages converts a list of Discordgo message structs to the message struct used by the archiver
func DgoToDcMessages(dg []*discordgo.Message) []*Message {
	dc := make([]*Message, len(dg))
	for i := range dg {
		dc[i] = DgoToDcMessage(dg[i])
	}
	return dc
}

// DgoToDcMessage converts a Discordgo message struct to the message struct used by the archiver
func DgoToDcMessage(dg *discordgo.Message) *Message {
	return &Message{
		ID:               dg.ID,
		ChannelID:        dg.ChannelID,
		GuildID:          dg.GuildID,
		Content:          dg.Content,
		Timestamp:        dg.Timestamp,
		EditedTimestamp:  dg.EditedTimestamp,
		TTS:              dg.TTS,
		Pinned:           dg.Pinned,
		MentionRoles:     dg.MentionRoles,
		MentionEveryone:  dg.MentionEveryone,
		MentionChannels:  DgoToDcChannels(dg.MentionChannels),
		MentionUsers:     DgoToDcUsers(dg.Mentions),
		Author:           DgoToDcUser(dg.Author),
		Attachments:      dg.Attachments,
		Embeds:           dg.Embeds,
		Reactions:        dg.Reactions,
		Type:             dg.Type,
		WebhookID:        dg.WebhookID,
		Activity:         dg.Activity,
		Application:      dg.Application,
		MessageReference: dg.MessageReference,
		Flags:            dg.Flags,
	}
}

// DgoToDcUser converts a Discordgo user struct to the darchive user struct
func DgoToDcUser(dg *discordgo.User) *User {
	return &User{
		ID:            dg.ID,
		Username:      dg.Username,
		Avatar:        dg.Avatar,
		Discriminator: dg.Discriminator,
		Bot:           dg.Bot,
	}
}

// DgoToDcUsers is like DgoToDcUser except plural, it takes a list of users
func DgoToDcUsers(dg []*discordgo.User) []*User {
	dc := make([]*User, len(dg))
	for i := range dg {
		dc[i] = DgoToDcUser(dg[i])
	}
	return dc
}

// DgoToDcChannel converts a discordgo channel struct to a darchive one
func DgoToDcChannel(dg *discordgo.Channel) *Channel {
	return &Channel{
		ID:               dg.ID,
		GuildID:          dg.GuildID,
		Name:             dg.Name,
		Topic:            dg.Topic,
		Type:             dg.Type,
		NSFW:             dg.NSFW,
		ParentID:         dg.ParentID,
		RateLimitPerUser: dg.RateLimitPerUser,
	}
}

// DgoToDcChannels converts a slice of discordgo channels to a slice of darchive ones
func DgoToDcChannels(dg []*discordgo.Channel) []*Channel {
	dc := make([]*Channel, len(dg))
	for i := range dg {
		dc[i] = DgoToDcChannel(dg[i])
	}
	return dc
}
