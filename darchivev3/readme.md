# Version 3 of Darchive

In V3 all contents are stored in a json file that may optionally be GZipped
It stores the date and channel of the archive and a list of messages within the archive

Be aware that the array of messages are in reverse order as compared to how they are displayed in chat, the top of the array contains the newest message and the bottom of the array contains the oldest message

The structs used for messages, channels, and users, are fairly typical to discord and are for the most part directly comparable to the data layout for messages used by discord itself