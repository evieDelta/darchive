package darchivev3

import (
	"encoding/json"
	"time"
)

// Version is just an int that always marshals to the FormatVersion, in this case 3
type Version int

// MarshalJSON marshals 3 in json
func (v Version) MarshalJSON() ([]byte, error) {
	return json.Marshal(PackVersion)
}

// PackVersion is the version of the archive format, would be more accurately referred to as formatVersion but darchive v3 is already frozen
const PackVersion int = 3

// ArchiveData contains the channel archive information
type ArchiveData struct {
	PackVersion Version   `json:"packVersion"` // note that via the data type this will always be marshaled to 4
	Date        time.Time `json:"timestamp"`

	Messages []*Message `json:"messages,omitempty"`
	Channel  *Channel   `json:"channel"`
}

// ArchiveDataEncoding is an alternate version of ArchiveData that replaces the Messages field with a json.RawMessage for manual constructing of the messages json, used in the mojitree archiver to be able to build the messages field in chunks instead of all at once
type ArchiveDataEncoding struct {
	PackVersion Version   `json:"packVersion"`
	Date        time.Time `json:"timestamp"`

	Messages json.RawMessage `json:"messages,omitempty"`
	Channel  *Channel        `json:"channel"`
}
