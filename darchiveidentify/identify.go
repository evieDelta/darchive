package darchiveidentify

import "encoding/json"

type identifications struct {
	PackVersion int `json:"packVersion"`
}

// Identify tries to figure out what version a package is, note that if you've got a compressed file you have to uncompress it before feeding it into here
func Identify(data []byte) (int, error) {
	ver := identifications{}
	err := json.Unmarshal(data, &ver)
	if err != nil {
		return 0, err
	}
	if ver.PackVersion != 0 {
		return ver.PackVersion, nil
	}
	return 0, nil
}
