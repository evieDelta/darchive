package darchive

import (
	"bytes"
	"compress/gzip"
	"io"
	"io/ioutil"

	"github.com/pkg/errors"
)

// GzipWriter writes some gzipped data into an io.Writer at max compression
func GzipWriter(data []byte, wr io.Writer) error {
	gz, err := gzip.NewWriterLevel(wr, gzip.BestCompression)
	if err != nil {
		return errors.Wrap(err, "failed to create gzip writer")
	}
	_, err = gz.Write(data)
	if err != nil {
		return errors.Wrap(err, "failed to compress data")
	}
	return errors.Wrap(gz.Close(), "failed to close gzip footer")
}

// Gzip some data
func Gzip(data []byte) ([]byte, error) {
	buf := bytes.NewBuffer(make([]byte, 0, len(data)/4))
	err := GzipWriter(data, buf)
	return buf.Bytes(), err
}

// UngzipWriter ungzips some data from an io.Reader
func UngzipWriter(rd io.Reader) ([]byte, error) {
	gz, err := gzip.NewReader(rd)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create gzip reader")
	}
	d, err := ioutil.ReadAll(gz)
	if err != nil {
		return d, errors.Wrap(err, "failed to read all gzipped data")
	}
	return d, nil
}

// Ungzip some data
func Ungzip(data []byte) ([]byte, error) {
	return UngzipWriter(bytes.NewReader(data))
}
